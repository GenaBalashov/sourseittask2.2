package com.company;

public class Main {

    public static void main(String[] args) {
        int a = 3, b = 10, c = 4;
        double xOne = 0, xTwo = 0, x = 0;
        double Diskriminant = 0;
        Diskriminant = b * b - 4 * a * c;
        if (Diskriminant > 0) {
            xOne = ((-b + Math.sqrt(Diskriminant)) / (2 * a));
            xTwo = ((-b - Math.sqrt(Diskriminant)) / (2 * a));
            System.out.println("Корни уравнения D > 0:");
            System.out.println("X1 = " + xOne);
            System.out.println("X2 = " + xTwo);
        } else if (Diskriminant == 0) {
            x = -b / (2 * a);
            System.out.println("Корни уравнения D = 0:");
            System.out.println("X = " + x);
        } else if (Diskriminant < 0) {
            System.out.println("Действительных корней нету.");
        }
    }
}
